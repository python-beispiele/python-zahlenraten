input("Denk dir eine Zahl zwischen 0 und 100 aus und bestätige mit ENTER")

print("Ich rate jetzt die Zahl und wenn sie gtößer ist, drücke >, wenn sie kleiner ist, drücke < und wenn sie richtig ist, drücke = und bestätige deine Eingabe mit ENTER")

untergrenze = 0
obergrenze = 100
zaehler = 0
eingabe = ""

zahl = 0
while eingabe != "=":
    zaehler = zaehler + 1
    zahl = round((untergrenze + obergrenze)/2)
    eingabe = input(str(zaehler) + ". Ist die Zahl " + str(zahl) + "? ")
    if eingabe == "<":
        obergrenze = zahl
    elif eingabe == ">":
        untergrenze = zahl
print("Ich habe " + str(zaehler) + " Versuche gebraucht")
print("Die Zahl ist ", zahl)