# Python Zahlenraten

Der Spieler denkt sich eine Zahl aus und der Computer soll sie raten.

# Aufgabe 1

Probiere alle Zahlen durch

# Lösung 1

[Link auf Commit](https://gitlab.com/python-beispiele/python-zahlenraten/-/commit/d0b5e2d35567bbc476cf527c8ece428e435f8ee5)

# Aufgabe 2

Teile den Suchraum in der Mitte auf und frage auch ob die gesuchte Zahl größer oder kleiner ist als die geratene Zahl-

# Lösung 2

[Link auf Commit](https://gitlab.com/python-beispiele/python-zahlenraten/-/commit/f751a42b3192075dbbfaf24e0f0c5a371a076945)

# Aufgabe 3

Teste beide Ansätze mit dem Bereich 1-1000 und 1-1000000 ;-)